<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add product</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <script type="text/javascript" src="assets/scripts/scripts.js"></script>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div id="page add-page">
        <header>
            <h1>Product Add</h1>
            <div class="header_buttons">
                <a href="./">
                    <button>Cancel</button>
                </a>
            </div>
        </header>
        <form id="product_form" action="./functions/insert.php" method="post">
            <div class="input_wrapper">
                <label for="sku">SKU</label>
                <input type="text" id="sku" name="sku">
                <p class="error"></p>
            </div>
            <div class="input_wrapper">
                <label for="name">Name</label>
                <input type="text" id="name" name="name">
                <p class="error"></p>
            </div>
            <div class="input_wrapper">
                <label for="price">Price ($)</label>
                <input type="text" id="price" name="price">
                <p class="error"></p>
            </div>
            <select id="productType" name="typeswitcher">
                <option>Type Switcher</option>
                <option id="DVD">DVD</option>
                <option id="Book">Book</option>
                <option id="Furniture">Furniture</option>
            </select>
            <p class="error"></p>
            <div id="attributes" class="input_wrapper">
                <p>Please select product type</p>
            </div>
            <button type="submit">Save</button>
        </form>
        <footer>
            Scandiweb Test assignment
        </footer>
    </div>
</body>
</html>