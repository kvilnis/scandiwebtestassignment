<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Scandiweb Test Task</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <script type="text/javascript" src="assets/scripts/scripts.js"></script>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div id="page list-page">
        <header>
            <h1>Product List</h1>
            <div class="header_buttons">
                <button onclick="location.href='./add-product';">ADD</button>
                <button id="delete-product-btn">MASS DELETE</button>
            </div>
        </header>
        <div class="product-list">
            <?php include 'functions/productList.php';?>
        </div>
        <footer>
            Scandiweb Test assignment
        </footer>
    </div>
</body>
</html>