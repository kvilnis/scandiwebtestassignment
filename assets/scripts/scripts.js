$(function(){

    // Disallow spaces from all inputs (except name) function

    function spaceCheck() {
        $('#product_form .input_wrapper').children('input').each(function () {
            if (!$(this).is('#name')) {
                $(this).keypress(function( e ) {
                    if(e.which === 32) {
                        $(this).next().html('Space is not allowed');
                        $(this).addClass('input_error');
                        return false;
                    }
                });
            }
        });
    }

    // Disallow spaces on page load

    spaceCheck();

    // Product type switcher ajax

    $('#productType').on('change', function () {
        if (this.value !== 'Type Switcher') {
            var switcherValue = this.value;
            $.ajax({
                url: 'functions/typeSwitcher.php',
                type: 'post',
                dataType: 'html',
                data: { typeswitcher: switcherValue },
                success: function (html) {
                    $('#attributes').html(html);

                    // Disallow spaces after switching product type

                    spaceCheck();
                }
            });
        } else {
            $('#attributes').html('<p>Please select product type</p>');
        }
    });


    // 'Add product' form validation

    $('#product_form').on('submit', function (e) {
        let submit = true;

        function input_error(input, $text) {
            submit = false;
            input.next().html($text);
            input.addClass('input_error');
        }

        function input_accepted(input) {
            input.next().html('');
            input.removeClass('input_error');
        }

        // Type switcher must have a product type selected

        if ($(this).children('#productType').val() === "Type Switcher") {
            submit = false;
            $('#attributes').children('p').css('color', 'red');
        } else {

            // All inputs have to be filled

            $('#product_form .input_wrapper').children('input').each(function () {
                let input = $(this);
                if (!input.val()) {
                    input_error(input, 'Please, submit required data');
                } else {
                    input_accepted(input);

                    // Check specific input fields for data types

                    if (input.is('#sku')) {
                        if ((input.val().length > 11)) {
                            input_error(input, 'SKU must be 11 characters or less');
                        } else {
                            input_accepted(input);
                            let sku = input.val();

                            // Check if SKU code already exists

                            $.ajax({
                                async: false,
                                url: 'functions/skuCheck.php',
                                type: 'post',
                                data: { sku: sku },
                                success: function (response) {
                                    if (response === 'false') {
                                        input_error(input, 'SKU already exists');
                                    } else {
                                        input_accepted(input);
                                    }
                                }
                            });
                        }
                    } else if (input.is('#name')) {
                        input.val($.trim(input.val()));
                        if (input.val().length > 20 || input.val().length < 3) {
                            input_error(input, 'Name must be from 3 to 20 characters');
                        } else {
                            input_accepted(input);
                        }
                    } else if (input.is('#price')) {
                        if (isNaN(input.val())) {
                            input_error(input, 'Please, provide the data of indicated type');
                        } else if (input.val().length > 11) {
                            input_error(input, 'Price must be less than 11 characters');
                        } else if (parseFloat(input.val()) <= 0) {
                            input_error(input, "Price can't be zero or negative");
                        } else {
                            input_accepted(input);
                        }
                    } else if (input.hasClass('attribute')) {
                        if (isNaN(input.val())) {
                            input_error(input, 'Please, provide the data of indicated type');
                        } else if (input.val().length > 5) {
                            input_error(input, 'Input must be 5 characters or less');
                        } else if (parseInt(input.val()) <= 0) {
                            input_error(input, "This input can't be zero or negative");
                        } else {
                            input_accepted(input);
                        }
                    }
                }
            });
        }

        if (submit === false) {
            e.preventDefault();
        }
    });

    // Mass delete

    $('#delete-product-btn').on('click', function () {
        let deleteProducts = [];
        let listItem = $('.product-list .product-list__item');
        if (listItem.length) {
            listItem.each(function () {
                let checkbox = $(this).children('input:checkbox');
                if (checkbox.prop('checked')) {
                    deleteProducts.push("'" + checkbox.val() + "'");
                }
            });
            $.ajax({
                url: 'functions/massDelete.php',
                type: 'post',
                data: { delProd: deleteProducts },
                success: function () {
                    location.reload();
                }
            });
        }
    });
});