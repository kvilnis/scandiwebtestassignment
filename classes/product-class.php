<?php

abstract class Product {
    function listRenderParent($sku, $name, $price) {
        echo '<div class="product-list__item">';
        echo '<input class="delete-checkbox" type="checkbox" value="' . $sku . '">';
        echo '<p>' . $sku . '</p>';
        echo '<p>' . $name . '</p>';
        echo '<p>' . $price . ' $' . '</p>';
    }
    abstract public function listRender($sku, $name, $price, $attr);
    abstract public function formRender();
}

class DVD extends Product {
    function listRender($sku, $name, $price, $attr){
        parent::listRenderParent($sku, $name, $price);
        echo '<p>Size: ' . $attr . ' MB</p></div>';
    }
    function setAttr() {
        return $_POST["size"];
    }
    function formRender() { ?>
        <p>Please, provide size in megabytes</p>
        <label for="size">Size (MB)</label>
        <input type="text" id="size" class="attribute" name="size">
        <p class="error"></p>
    <?php }
}

class Book extends Product {
    function listRender($sku, $name, $price, $attr){
        parent::listRenderParent($sku, $name, $price);
        echo '<p>Weight: ' . $attr . ' KG</p></div>';
    }
    function setAttr(){
        return $_POST["weight"];
    }
    function formRender() { ?>
        <p>Please, provide weight in kilograms</p>
        <label for="weight">Weight (KG)</label>
        <input type="text" id="weight" class="attribute" name="weight">
        <p class="error"></p>
    <?php }
}

class Furniture extends Product {
    function listRender($sku, $name, $price, $attr){
        parent::listRenderParent($sku, $name, $price);
        echo '<p>Dimensions: ' . $attr . ' (CM)</p></div>';
    }
    function setAttr(){
        return $_POST["height"] . 'x' . $_POST["width"] . 'x' . $_POST["length"];
    }
    function formRender() { ?>
        <p>Please, provide dimensions in centimetres</p>
        <label for="height">Height (CM)</label>
        <input type="text" id="height" class="attribute" name="height">
        <p class="error"></p>
        <label for="width">Width (CM)</label>
        <input type="text" id="width" class="attribute" name="width">
        <p class="error"></p>
        <label for="length">Length (CM)</label>
        <input type="text" id="length" class="attribute" name="length">
        <p class="error"></p>
    <?php }
}