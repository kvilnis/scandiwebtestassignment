<?php
include_once 'product-class.php';

class Queries {

    function saveProduct() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "scandiweb";

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // prepare sql and bind parameters
            $stmt = $conn->prepare("INSERT INTO products (sku, name, price, attributes, classname) VALUES (:sku, :name, :price, :attributes, :classname)");
            $stmt->bindParam(':sku', $sku);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':price', $price);
            $stmt->bindParam(':attributes', $attributes);
            $stmt->bindParam(':classname', $classname);

            // insert a row
            $sku = $_POST["sku"];
            $name = $_POST["name"];
            $price = $_POST["price"];
            $classname = $_POST["typeswitcher"];
            $product = new $classname();
            $attributes = $product->setAttr();
            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
        header("Location: ../.",TRUE,302);
    }

    function skuCheck() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "scandiweb";

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sku = $_POST["sku"];

            $sql = "SELECT sku FROM products WHERE sku = '$sku'";
            $stmt = $conn->query($sql);

            $stmt->execute();

            $response = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($response) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
        catch(PDOException $e)
        {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }

    function renderProductList() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "scandiweb";

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sql = "SELECT * FROM products ORDER BY product_key DESC";
            $stmt = $conn->query($sql);

            $stmt->execute();

            $response = $stmt->fetchAll();

            if ($response) {
                foreach ($response as $row) {
                    $classname = $row['classname'];
                    $product = new $classname();
                    $product->listRender($row['sku'], $row['name'], $row['price'], $row['attributes']);
                }
            } else {
                echo '<p class="product-list__empty">No products available</p>';
            }

        }
        catch(PDOException $e)
        {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }

    function massDelete() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "scandiweb";

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $delProd = implode(',', $_POST["delProd"]);

            $sql = "DELETE FROM products WHERE sku IN (" . $delProd . ")";
            $stmt = $conn->query($sql);

            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
}